const test = require('tape');
const unified = require('unified');
const remarkParse = require('remark-parse');
const inspect = require('unist-util-inspect');
const addListMetadata = require('./index');

test('it transforms the AST', t => {
  t.plan(2);

  const markdown = `
# Title

![red car](./red-car.jpeg)

- First list item
- Second list item
  - ordered list:
    1. one
    2. two
- Third list item
`;

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  // console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'paragraph',
        children: [
          {
            type: 'image',
            title: null,
            url: './red-car.jpeg',
            alt: 'red car',
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 4, column: 27, offset: 36},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 27, offset: 36},
          indent: [],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        spread: false,
        children: [
          {
            type: 'listItem',
            spread: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'First list item',
                    position: {
                      start: {line: 6, column: 3, offset: 40},
                      end: {line: 6, column: 18, offset: 55},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 6, column: 3, offset: 40},
                  end: {line: 6, column: 18, offset: 55},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 6, column: 1, offset: 38},
              end: {line: 6, column: 18, offset: 55},
              indent: [],
            },
          },
          {
            type: 'listItem',
            spread: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'Second list item',
                    position: {
                      start: {line: 7, column: 3, offset: 58},
                      end: {line: 7, column: 19, offset: 74},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 7, column: 3, offset: 58},
                  end: {line: 7, column: 19, offset: 74},
                  indent: [],
                },
              },
              {
                type: 'list',
                ordered: false,
                start: null,
                spread: false,
                children: [
                  {
                    type: 'listItem',
                    spread: false,
                    checked: null,
                    children: [
                      {
                        type: 'paragraph',
                        children: [
                          {
                            type: 'text',
                            value: 'ordered list:',
                            position: {
                              start: {line: 8, column: 5, offset: 79},
                              end: {line: 8, column: 18, offset: 92},
                              indent: [],
                            },
                          },
                        ],
                        position: {
                          start: {line: 8, column: 5, offset: 79},
                          end: {line: 8, column: 18, offset: 92},
                          indent: [],
                        },
                      },
                      {
                        type: 'list',
                        ordered: true,
                        start: 1,
                        spread: false,
                        children: [
                          {
                            type: 'listItem',
                            spread: false,
                            checked: null,
                            children: [
                              {
                                type: 'paragraph',
                                children: [
                                  {
                                    type: 'text',
                                    value: 'one',
                                    position: {
                                      start: {line: 9, column: 8, offset: 100},
                                      end: {line: 9, column: 11, offset: 103},
                                      indent: [],
                                    },
                                  },
                                ],
                                position: {
                                  start: {line: 9, column: 8, offset: 100},
                                  end: {line: 9, column: 11, offset: 103},
                                  indent: [],
                                },
                              },
                            ],
                            position: {
                              start: {line: 9, column: 5, offset: 97},
                              end: {line: 9, column: 11, offset: 103},
                              indent: [],
                            },
                          },
                          {
                            type: 'listItem',
                            spread: false,
                            checked: null,
                            children: [
                              {
                                type: 'paragraph',
                                children: [
                                  {
                                    type: 'text',
                                    value: 'two',
                                    position: {
                                      start: {line: 10, column: 8, offset: 111},
                                      end: {line: 10, column: 11, offset: 114},
                                      indent: [],
                                    },
                                  },
                                ],
                                position: {
                                  start: {line: 10, column: 8, offset: 111},
                                  end: {line: 10, column: 11, offset: 114},
                                  indent: [],
                                },
                              },
                            ],
                            position: {
                              start: {line: 10, column: 5, offset: 108},
                              end: {line: 10, column: 11, offset: 114},
                              indent: [],
                            },
                          },
                        ],
                        position: {
                          start: {line: 9, column: 5, offset: 97},
                          end: {line: 10, column: 11, offset: 114},
                          indent: [5],
                        },
                      },
                    ],
                    position: {
                      start: {line: 8, column: 3, offset: 77},
                      end: {line: 10, column: 11, offset: 114},
                      indent: [3, 3],
                    },
                  },
                ],
                position: {
                  start: {line: 8, column: 3, offset: 77},
                  end: {line: 10, column: 11, offset: 114},
                  indent: [3, 3],
                },
              },
            ],
            position: {
              start: {line: 7, column: 1, offset: 56},
              end: {line: 10, column: 11, offset: 114},
              indent: [1, 1, 1],
            },
          },
          {
            type: 'listItem',
            spread: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'Third list item',
                    position: {
                      start: {line: 11, column: 3, offset: 117},
                      end: {line: 11, column: 18, offset: 132},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 11, column: 3, offset: 117},
                  end: {line: 11, column: 18, offset: 132},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 11, column: 1, offset: 115},
              end: {line: 11, column: 18, offset: 132},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 6, column: 1, offset: 38},
          end: {line: 11, column: 18, offset: 132},
          indent: [1, 1, 1, 1, 1],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 12, column: 1, offset: 133},
    },
  };
  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = addListMetadata()(actualInput);

  // console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'image',
        title: null,
        url: './red-car.jpeg',
        alt: 'red car',
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 4, column: 27, offset: 36},
          indent: [],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        spread: false,
        children: [
          {
            type: 'listItem',
            spread: false,
            checked: null,
            children: [
              {
                type: 'text',
                value: 'First list item',
                position: {
                  start: {line: 6, column: 3, offset: 40},
                  end: {line: 6, column: 18, offset: 55},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 6, column: 1, offset: 38},
              end: {line: 6, column: 18, offset: 55},
              indent: [],
            },
            index: 0,
            ordered: false,
          },
          {
            type: 'listItem',
            spread: false,
            checked: null,
            children: [
              {
                type: 'text',
                value: 'Second list item',
                position: {
                  start: {line: 7, column: 3, offset: 58},
                  end: {line: 7, column: 19, offset: 74},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 7, column: 1, offset: 56},
              end: {line: 10, column: 11, offset: 114},
              indent: [1, 1, 1],
            },
            index: 1,
            ordered: false,
          },
        ],
        position: {
          start: {line: 6, column: 1, offset: 38},
          end: {line: 11, column: 18, offset: 132},
          indent: [1, 1, 1, 1, 1],
        },
        depth: 0,
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        spread: false,
        children: [
          {
            type: 'listItem',
            spread: false,
            checked: null,
            children: [
              {
                type: 'text',
                value: 'ordered list:',
                position: {
                  start: {line: 8, column: 5, offset: 79},
                  end: {line: 8, column: 18, offset: 92},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 8, column: 3, offset: 77},
              end: {line: 10, column: 11, offset: 114},
              indent: [3, 3],
            },
            index: 0,
            ordered: false,
          },
        ],
        position: {
          start: {line: 8, column: 3, offset: 77},
          end: {line: 10, column: 11, offset: 114},
          indent: [3, 3],
        },
        depth: 1,
      },
      {
        type: 'list',
        ordered: true,
        start: 1,
        spread: false,
        children: [
          {
            type: 'listItem',
            spread: false,
            checked: null,
            children: [
              {
                type: 'text',
                value: 'one',
                position: {
                  start: {line: 9, column: 8, offset: 100},
                  end: {line: 9, column: 11, offset: 103},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 9, column: 5, offset: 97},
              end: {line: 9, column: 11, offset: 103},
              indent: [],
            },
            index: 0,
            ordered: true,
          },
          {
            type: 'listItem',
            spread: false,
            checked: null,
            children: [
              {
                type: 'text',
                value: 'two',
                position: {
                  start: {line: 10, column: 8, offset: 111},
                  end: {line: 10, column: 11, offset: 114},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 10, column: 5, offset: 108},
              end: {line: 10, column: 11, offset: 114},
              indent: [],
            },
            index: 1,
            ordered: true,
          },
        ],
        position: {
          start: {line: 9, column: 5, offset: 97},
          end: {line: 10, column: 11, offset: 114},
          indent: [5],
        },
        depth: 2,
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        spread: false,
        children: [
          {
            type: 'listItem',
            spread: false,
            checked: null,
            children: [
              {
                type: 'text',
                value: 'Third list item',
                position: {
                  start: {line: 11, column: 3, offset: 117},
                  end: {line: 11, column: 18, offset: 132},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 11, column: 1, offset: 115},
              end: {line: 11, column: 18, offset: 132},
              indent: [],
            },
            index: 2,
            ordered: false,
          },
        ],
        position: {
          start: {line: 6, column: 1, offset: 38},
          end: {line: 11, column: 18, offset: 132},
          indent: [1, 1, 1, 1, 1],
        },
        depth: 0,
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 12, column: 1, offset: 133},
    },
  };

  t.deepEquals(actualOutput, expectedOutput, 'output looks good');
});
