# mdast-normalize-react-native

Applies transformations to the markdown AST ([MDAST](https://github.com/syntax-tree/mdast)) to make it easier to render in React Native, because React Native (on Android) has some restrictions (such as no `<View>` inside `<Text>`) which makes it tricky to render markdown in native views. This package uses a few plugins that will create an AST friendly for rendering constraints in React Native.

Utilizes:

- mdast-add-list-metadata
  - Adds some useful metadata that can be used during rendering, for instance...
  - `depth` on list nodes indicates the depth of nesting
  - `index` on list item nodes indicates the ordering of the item
  - `ordered` on list item nodes indicates whether the parent list is ordered
- mdast-flatten-nested-lists
  - Guarantees no list inside a list item
- mdast-flatten-listitem-paragraphs
  - Collapses `listItem > paragraph > *` to `listItem > *`
- mdast-flatten-image-paragraphs
  - Collapses `paragraph > image` to `image`
- mdast-move-images-to-root
  - Moves `image` nodes up the tree until they are strict children of the root

## Installation

```sh
npm install mdast-normalize-react-native
```

## Usage

```js
// ...
var normalizeForReactNative = require('mdast-normalize-react-native');

unified()
  .use(parse)
  .use(normalizeForReactNative)
// ...
```

Markdown document:

```
# Title

![red car](./red-car.jpeg)

- First list item
- Second list item
  1. one
  2. two
- Third list item
```

Input AST:

```
root[3] (1:1-11:1, 0-111)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
├─ paragraph[1] (4:1-4:27, 10-36)
│  └─ image (4:1-4:27, 10-36) [url="./red-car.jpeg"][alt="red car"]
└─ list[3] (6:1-10:18, 38-110) [ordered=false][loose=false]
   ├─ listItem[1] (6:1-6:18, 38-55) [loose=false]
   │  └─ paragraph[1] (6:3-6:18, 40-55)
   │     └─ text: "First list item" (6:3-6:18, 40-55)
   ├─ listItem[2] (7:1-9:9, 56-92) [loose=false]
   │  ├─ paragraph[1] (7:3-7:19, 58-74)
   │  │  └─ text: "Second list item" (7:3-7:19, 58-74)
   │  └─ list[2] (8:3-9:9, 77-92) [ordered=true][start=1][loose=false]
   │     ├─ listItem[1] (8:3-8:9, 77-83) [loose=false]
   │     │  └─ paragraph[1] (8:6-8:9, 80-83)
   │     │     └─ text: "one" (8:6-8:9, 80-83)
   │     └─ listItem[1] (9:3-9:9, 86-92) [loose=false]
   │        └─ paragraph[1] (9:6-9:9, 89-92)
   │           └─ text: "two" (9:6-9:9, 89-92)
   └─ listItem[1] (10:1-10:18, 93-110) [loose=false]
      └─ paragraph[1] (10:3-10:18, 95-110)
         └─ text: "Third list item" (10:3-10:18, 95-110)
```

Output AST:

```
root[5] (1:1-11:1, 0-111)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
├─ image (4:1-4:27, 10-36) [url="./red-car.jpeg"][alt="red car"]
├─ list[2] (6:1-10:18, 38-110) [ordered=false][loose=false][depth=0]
│  ├─ listItem[1] (6:1-6:18, 38-55) [loose=false][index=0][ordered=false]
│  │  └─ text: "First list item" (6:3-6:18, 40-55)
│  └─ listItem[1] (7:1-9:9, 56-92) [loose=false][index=1][ordered=false]
│     └─ text: "Second list item" (7:3-7:19, 58-74)
├─ list[2] (8:3-9:9, 77-92) [ordered=true][start=1][loose=false][depth=1]
│  ├─ listItem[1] (8:3-8:9, 77-83) [loose=false][index=0][ordered=true]
│  │  └─ text: "one" (8:6-8:9, 80-83)
│  └─ listItem[1] (9:3-9:9, 86-92) [loose=false][index=1][ordered=true]
│     └─ text: "two" (9:6-9:9, 89-92)
└─ list[1] (6:1-10:18, 38-110) [ordered=false][loose=false][depth=0]
   └─ listItem[1] (10:1-10:18, 93-110) [loose=false][index=2][ordered=false]
      └─ text: "Third list item" (10:3-10:18, 95-110)
```

## License

[MIT](LICENSE)