const addListMetadata = require('mdast-add-list-metadata')();
const flattenNestedLists = require('mdast-flatten-nested-lists')();
const flattenImageParagraphs = require('mdast-flatten-image-paragraphs')();
const flattenListItemParagraphs = require('mdast-flatten-listitem-paragraphs')();
const moveImagesToRoot = require('mdast-move-images-to-root')();

function normalizeForReactNative() {
  return ast =>
    moveImagesToRoot(
      flattenImageParagraphs(
        flattenListItemParagraphs(flattenNestedLists(addListMetadata(ast))),
      ),
    );
}

module.exports = normalizeForReactNative;
